package com.algorithmtraining;

import com.algorithmtraining.controller.SampleController;
import org.springframework.boot.SpringApplication;

public class Application {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(SampleController.class, args);
    }
}
